#ifndef __SpaceTest__
#define __SpaceTest__

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cmath>

using namespace std;
/**
* compare float float
*/
bool compareFloat(double a, double b) {
    double epsilon = 0.00001;
    return fabs(a-b) < epsilon;
}

namespace SpaceTest {

    static void t(double a, double b, string s = "") {
        bool ret = compareFloat(a, b);
        if(ret)
            printf("true => %s\n", s.c_str());
        else{
            printf("a=%f b=%f\n", a, b);
            printf("false => %s\n", s.c_str());
        }
        printf("\n");
    }
    static void t(int a, int b, string s = "") {
       if(a == b) 
           printf("true => %s", s.c_str());
       else{
           printf("a=%d b=%d\n", a, b);
           printf("false => %s\n", s.c_str());
       }
        printf("\n");
    }
    static void t(long a, long b, string s = "") {
       if(a == b) 
           printf("true => %s", s.c_str());
       else{
           printf("a=%l, b=%l\n", a, b);
           printf("false => %s\n", s.c_str());
       }
        printf("\n");
    }
    static void t(bool a, bool b, string s = "") {
        bool bb = a == b;
       if(a == b) 
           printf("Ture => %s  %s\n", bb ? "True" : "False", s.c_str());
       else{
           printf("a=%s b=%s\n", a ? "True" : "False", b ? "True" : "False");
           printf("False => %s\n", s.c_str());
       }
        printf("\n");
    }
    static void t(string a, string b, string s = "") {
       if(a == b) 
           printf("true => %s\n", s.c_str());
       else{
           printf("a = %s  b = %s\n", a.c_str(), b.c_str());
           printf("false => %s\n", s.c_str());
       }
        printf("\n");
    }
};  // end SpaceTest 
#endif

