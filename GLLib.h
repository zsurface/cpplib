#ifndef __GLLIB__ 
#define __GLLIB__

#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <cmath>


void printFormat(GLfloat x, GLfloat y, char *format,...) {
    va_list args;
    char buffer[200], *p;

    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);
    glPushMatrix();
    glTranslatef(x, y, 0);
    for (p = buffer; *p; p++)
        glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);
    glPopMatrix();
}

void printFormatNew(GLfloat x, GLfloat y, char* buffer) {
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();

    // l          r
    // ------------ up
    // |          |
    // |          |
    // ------------ bottom
    //
    // left, right, bottom, up
    gluOrtho2D( 0, 1280, 0, 1024 );

    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x, y);

    for ( int i = 0; i < strlen(buffer); i++) {
        //glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, buffer[i]);
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
    }

    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
}
void printFormat(GLfloat x, GLfloat y, GLfloat fl, GLfloat f2) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    char buffer1[50];
    char buffer2[50];
    sprintf(buffer1, "%f", fl);
    for (char* p = buffer1; *p; p++)
        glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);

    glTranslatef(x + 30.0, 0.0, 0);
    sprintf(buffer2, "%f", f2);
    for (char* p = buffer2; *p; p++)
        glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);
    glPopMatrix();
}

void printFormat(GLfloat x, GLfloat y, GLfloat fl) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    char buffer[50];
    sprintf(buffer, "%f", fl);
    for (char* p = buffer; *p; p++)
        glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);
    glPopMatrix();
}
void getModelViewMatrix(float matrix[16]) {
    glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
    fprintf(stderr, "[%s]\n", "Algebra Matrix Form");
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {
            // OpenGL use column-major instead of row-major
            fprintf(stderr, "[%f]", matrix[4*j+i]);
        }
        fprintf(stderr, "%s", "\n");
    }
    fprintf(stderr, "%s", "\n");
}
#endif
