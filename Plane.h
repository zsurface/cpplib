#ifndef PLANE 
#define PLANE 

#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <cmath>

class Plane {
public:
    float x;
    float y;
    Plane(int x_ = 1.0f, int y_ = 1.0f) {
        x = x_;
        y = y_;
    }
// draw x-y plane
    void draw() {
        float alpha = 0.5;
        glBegin(GL_QUADS);
        glColor4f(x, 0.0, 0.0, alpha);
        glVertex3f(-x, +y, 0.0); // top left

        glColor4f(0.0, y, 0.0, alpha);
        glVertex3f(-x, -y, 0.0); // bottom left

        glColor4f(0.0, 0.0, 1.0, alpha);
        glVertex3f(+x, -y, 0.0); // bottom right

        glColor4f(0.0, y, 1.0, alpha);
        glVertex3f(+x, +y, 0.0); // top right
        glEnd();
    }
}; // end class Plane


#endif
