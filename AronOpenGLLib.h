#ifndef __AronOpenGLLib__
#define __AronOpenGLLib__

#include <execinfo.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cmath>

#include <math.h>
#include "bitmap_image.hpp"
#include <GLUT/glut.h>  /* Header File For The GLut Library*/
#include "AronLib.h"


using namespace std;

/**
    Save framebuffer to bmp file
    Use following lib:
    https://bitbucket.org/zsurface/cpplib/raw/3a47ae0e95a1ee93f7b4307411eae2b178fb1665/bitmap_image.hpp

    /Users/cat/myfile/bitbucket/opengl/DrawLine/drawline.cpp
    https://bitbucket.org/zsurface/opengl/raw/02f235246fa889eac79142be959ba63bdb2ced23/DrawLine/drawline.cpp

    include "bitmap_image.hpp"
    using namespace std;
*/
void sameImageBMP(int width, int height, string path){
    // bitmap stuff from bitmap_image.hpp
	unsigned char* imageData = (unsigned char *)malloc((int)(width*height*(3)));
	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, imageData);
	bitmap_image image(width,height);
	image_drawer draw(image);
    
	for (unsigned int i = 0; i < image.width(); ++i) {
		for (unsigned int j = 0; j < image.height(); ++j) {
			image.set_pixel(i,j,*(++imageData),*(++imageData),*(++imageData));		
		}
	}
	image.save_image(path);
}

void printFormatNew(GLfloat x, GLfloat y, char* buffer) {
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();

    // l          r
    // ------------ up
    // |          |
    // |          |
    // ------------ bottom
    //
    // left, right, bottom, up
    gluOrtho2D( 0, 1280, 0, 1024 );

    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x, y);

    for ( int i = 0; i < strlen(buffer); i++) {
        //glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, buffer[i]);
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
    }

    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
}

class SimpleCoordinate {
public:
    SimpleCoordinate() {
    }
public:
    void draw(float width = 1.0, int num=10) {
        glBegin(GL_LINES);
        float delta = width/num;
        glColor3f(0.0f, width, 0.0f);
        // XZ-plan
        for(int i=-num; i<=num; i++) {
            glVertex3f(-width, 0.0f, delta*i);
            glVertex3f(width, 0.0f,  delta*i);
        }
        // XY-plan
        glColor3f(0.3f,0.7f,0.0f);
        for(int i=-num; i<=num; i++) {
            glVertex3f(-width, delta*i,  0.0f);
            glVertex3f(width,  delta*i,  0.0f);
        }
        // YZ-plan
        glColor3f(width, 0.0f,0.0f);
        for(int i=-num; i<=num; i++) {
            glVertex3f(0.0f, -width, delta*i);
            glVertex3f(0.0f, width,  delta*i);
        }

        glColor3f(0.4f, 0.4f,0.1f);
        for(int i=-num; i<=num; i++) {
            glVertex3f(delta*i, -width, 0.0f);
            glVertex3f(delta*i, width,  0.0f);
        }

        glColor3f(0.0f, 0.0f, width);
        for(int i=-num; i<=num; i++) {
            glVertex3f(delta*i, 0.0f, -width);
            glVertex3f(delta*i, 0.0f, width);
        }

        glColor3f(0.0f, 0.5f, 0.5f);
        for(int i=-num; i<=num; i++) {
            glVertex3f(0.0f, delta*i, -width);
            glVertex3f(0.0f, delta*i, width);
        }
        glEnd();
    }

}; // end class SimpleCoordinate


template<typename Fun>
vector<float> curvePts(Fun f, int nPoint){
  vector<float> vx;
  vector<float> vy;
  float del = (float)1/nPoint;
  int half = (int)nPoint/2;
  int initVal = -half;

  for(int i = -half; i < half; i++){
    float val = del*i;
    add(vx, (float)val);
    add(vy, (float)f(val));
  }
  
  vector<float>  zerov = repeatVec(nPoint, (float)0.0);
  vector<float> vec = interleave<float>(vx, vy, zerov);

  return std::move(vec);
}

#endif

