#ifndef __CONST__ 
#define __CONST__ 

#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define PAD "--------------------------------------------------------------------------------"

GLfloat RED[3]     = {1, 0, 0};
GLfloat WHITE[3]   = {1, 1, 1};
GLfloat GREEN[3]   = {0, 1, 0};
GLfloat MAGENTA[3] = {1, 0, 1};

#endif
