#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libproc.h>

void multiply4f(float m1[4][4], float m2[4][4], float m3[4][4]){
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            float s = 0.0f;
            for(int k=0; k<4; k++){
                m3[k][j] += m1[k][i]*m2[i][j];
            }
        }
    }
}

void multiply16f(float m1[16], float m2[16], float m3[16]){
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            for(int k=0; k<4; k++){
                m3[4*k + j] += m1[4*k + i] * m2[4*i + j];
            }
        }
    }
}

void printMatrix2(const int size, const float m[size]){
    for(int i=0; i<size; i++){
        if((i + 1) % 4 == 0)
            printf("[%f]\n",m[i]);
        else
            printf("[%f]",m[i]);
    }
    printf("--------------------------------------\n");
}

void printMatrix(float m[4][4]){
    for(int i=0; i<4; i++){
        for(int j=0; j<4; j++){
            printf("[%f]", m[i][j]);
        }
        printf("\n");
    }
    printf("--------------------------------------\n");
}

// #define OK
#ifdef OK
int main (int argc, char* argv[]) {
    float ma1[4][4] = {
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12},
        {13, 14, 15, 16}
    };
    float ma2[4][4] = {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };
    float ma3[4][4] = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };

    float m11[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };

    float m22[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    float m33[16] = {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    float mm1[16] = {
        1,   2,  3, 4,
        5,   6,  7, 8,
        9,  10, 11, 12,
        13, 14, 15, 16 
    };

    multiply16f(m11, m22, m33);
    printMatrix2(16, m33);
    float mm2[16];
    multiply16f(mm1, mm1, mm2);
    printMatrix2(16, mm2);

    float ma4[4][4];
    multiply4f(ma1, ma1, ma4);
    printMatrix(ma4);

    printf("output");
    return 0;
}

#endif
